package com.java.christian.enheritance;

import com.java.christian.enheritance.vehicles.Car;
import com.java.christian.enheritance.vehicles.Submarine;

public class Main {



    public static void main(String[] args) {

        Car car = new Car();
        Submarine submarine = new Submarine();

        car.Accelerate();
        car.Move();
        car.Stop();
        car.aboutCar();
        submarine.implodes();

    }
}
