package com.java.christian.enheritance.vehicles;
import com.java.christian.enheritance.Vehicle;

public class Car extends Vehicle {

    String make = "Mitsubishi";
    String model = "FTO";
    double horsepower = 197;
    int maxRPM = 8600;

    public void aboutCar()  {
        System.out.println("The make is: "+ make);
        System.out.println("The model is: "+ model);
        System.out.println("The horsepower is: "+ horsepower);
        System.out.println("The max RPM is: "+ maxRPM);
        System.out.println("Mivec kicks in arround 5500 rpm !!");
    }

}
