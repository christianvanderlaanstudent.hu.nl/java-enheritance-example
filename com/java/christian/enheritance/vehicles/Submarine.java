package com.java.christian.enheritance.vehicles;

import com.java.christian.enheritance.Vehicle;

public class Submarine extends Vehicle {

    public void submerges() {
        System.out.println("the submarine is submerging");
    }

    public void raisePeriscope()    {

    }

    public void resurfaces()    {
        System.out.println("the submarine is resurfacing");
    }

    public void implodes()   {
        System.out.println("This submarine has imploded and has failed sucessfully");
    }
}
