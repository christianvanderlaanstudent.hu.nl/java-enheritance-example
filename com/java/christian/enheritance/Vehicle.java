package com.java.christian.enheritance;

public class Vehicle {

    private int speed;

    public void Accelerate(){
        System.out.println("This vehicle is Accelerating");
    }

    public void Stop()  {
        System.out.println("This vehicle is Stopping");
    }

    public void Move()  {
        System.out.println("This vehicle is moving");
    }

}
